package connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBuse {
		//public static Connection connect = null;
		private Connection myCon;
		

		private String URL;// = "jdbc:mysql://localhost:3306/students?autoReconnect=true&useSSL=false";
		private String USER;// = "root";
		private String PASS;// = "root";
		private String current_student_name="";
		private String current_course_name="";
		private String current_teacher_name="";
		private String current_student_address="";
		private String current_course_year="";
		
		public DBuse( String URL, String USER, String PASS){
			this.URL=URL;
			this.USER=USER;
			this.PASS=PASS;
		}

		public String connect(){			
			try{
				//1. Get connection to DB
				myCon = DriverManager.getConnection(URL, USER, PASS);
				System.out.println("Conectat!\n");
				return "connected";
				
			} catch(Exception e){
				e.printStackTrace();
				return "Failed to connect!";
			}
			
		}
		
		@SuppressWarnings("finally")
		public String complexStudentQuery( String query) {
			//2. Create a statement
			Statement myStmt;
			String output="";

				try {
					myStmt = myCon.createStatement();		
					//3. Execute a SQL query
					ResultSet res;
					res = myStmt.executeQuery("select * from student"+query);
					
					//4. Process the result set
					while (res.next()){
						System.out.println(res.getString("name")+" "+res.getString("birth")+" "+ res.getString("address"));
						output=output.concat(res.getString("name")+" "+res.getString("birth")+" "+ res.getString("address")+"\n");
						setCurrent_student_name(res.getString("name"));
						setCurrent_student_address(res.getString("address"));
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				finally{
					return output;
				}
		}
		
		public void simpleStudentQuery(){
			complexStudentQuery("");
		}
		
		@SuppressWarnings("finally")
		public String complexCourseQuery( String query) {
			//2. Create a statement
			Statement myStmt;
			String output="";
				try {
					myStmt = myCon.createStatement();		
					//3. Execute a SQL query
					ResultSet res;
					res = myStmt.executeQuery("select * from course"+query);
					
					//4. Process the result set
					while (res.next()){
						System.out.println(res.getString("name")+" "+res.getString("teacher")+" "+ res.getString("study_year"));
						output=output.concat(res.getString("name")+" "+res.getString("teacher")+" "+ res.getString("study_year")+"\n");
						setCurrent_course_name(res.getString("name"));
						setCurrent_teacher_name(res.getString("teacher"));
						setCurrent_course_year(res.getString("study_year"));
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				finally{
					return output;
				}
			}
		
		public void simpleCourseQuery(){
			complexCourseQuery("");
		}
		
		public boolean checkStudentExistance(String name){
			boolean ok=true;
			
			String res = complexStudentQuery(" where name='"+name+"';");
			if(res.contains(name))
			{

				ok=false;
				System.out.println("A student with this name already exists!\n");
			}
			//System.out.println("No item with this name!");
			return ok;
		}
		
		public boolean checkCourseExistance(String name){
			boolean ok=true;
			
			String res = complexCourseQuery(" where name='"+name+"';");
			if(res.contains(name))
			{
				ok=false;
				System.out.println("An item with this name already exists!");
			}
			else
				System.out.println("No item with this name!");
			return ok;
		}
		
		public int insertStudent(String name, String birthdate, String address){
			//2. Create a statement
			int res = 0;
			if(checkStudentExistance(name)==true){
				Statement myStmt;
				try {
					myStmt = myCon.createStatement();		
					//3. Execute a data manipulation query
					res = myStmt.executeUpdate("INSERT INTO `students`.`student` (`name`, `birth`, `address`) VALUES ('"+ name+"', '"+birthdate +"', '"+address+"');");
					System.out.println("Just inserted a student.");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return res;
		}

		
		public int insertCourse(String name, String teacher, int year){
			//2. Create a statement
			int res = 0;
			if(checkCourseExistance(name)==true){
				Statement myStmt;
				try {
					myStmt = myCon.createStatement();		
					//3. Execute a data manipulation query
					res = myStmt.executeUpdate("INSERT INTO `students`.`course` (`name`, `teacher`, `study_year`) VALUES ('"+ name+"', '"+teacher+"', '"+year+"');");
					System.out.println("Just inserted a course.");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return res;
		}
		
		public void deleteStudentByName(String name){
			// DELETE FROM `students`.`student` WHERE `name`='anamaria';
			//2. Create a statement
			if(checkStudentExistance(name)==false){
				Statement myStmt;
	
					try {
						myStmt = myCon.createStatement();		
						//3. Execute a data manipulation query
						int res;
						res = myStmt.executeUpdate("DELETE FROM `students`.`student` WHERE `name`='"+ name+"';");
						System.out.println("Just deleted a student.");
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}		
		}

		public void deleteCourseByName(String name){
			// DELETE FROM `students`.`student` WHERE `name`='anamaria';
			//2. Create a statement
			if(checkStudentExistance(name)==true){
				Statement myStmt;
				try {
						myStmt = myCon.createStatement();		
						//3. Execute a data manipulation query
						int res;
						res = myStmt.executeUpdate("DELETE FROM `students`.`course` WHERE `name`='"+ name+"';");
						System.out.println("Just deteled a course.");
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		
		public boolean checkEnrollment(int id1, int id2){
			boolean aux=true;
			Statement myStmt;
			ResultSet res = null;
			try {
					myStmt = myCon.createStatement();		
					//3. Execute a data manipulation query
					res = myStmt.executeQuery("select * from student_attends_course");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			try {
				while (res.next()){
						if(res.getInt("id_student")==id1 && res.getInt("id_course")==id2)
							aux=false;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return aux;
		}
		
		public void enrollClass(int studID, int courseID){
			//INSERT INTO `students`.`student_attends_course` (`id_student`, `id_course`) VALUES ('4', '1');
			Statement myStmt;
			try {
				myStmt = myCon.createStatement();		
				//3. Execute a data manipulation query
				int res;
				res = myStmt.executeUpdate("INSERT INTO `students`.`student_attends_course` (`id_student`, `id_course`) VALUES ('"+studID+"', '"+courseID+"');");
				System.out.println("A student just enrolled a course.");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		public void dropClass(int studID, int courseID){
			Statement myStmt;
			try {
				myStmt = myCon.createStatement();		
				//3. Execute a data manipulation query
				int res;
				res = myStmt.executeUpdate("DELETE FROM `students`.`student_attends_course` WHERE `id_student`='"+studID+"' AND `id_course`='"+courseID+"';");
				System.out.println("A student just dropped a course.");
			} catch (SQLException e) {
				System.out.println("Wtf dropping");
				e.printStackTrace();
			}
		}
		
		public int updateStudent(String name, String address){
			//UPDATE `students`.`student` SET `address`='Badon, Salaj, nr 119' WHERE
			Statement myStmt;
			int res = 0;
			if(checkStudentExistance(name)){
				try {
					myStmt = myCon.createStatement();		
					//3. Execute a data manipulation query
					res = myStmt.executeUpdate("UPDATE `students`.`student` SET `address`='"+address+"' WHERE name='"+name+"';");
					System.out.println("New info for a student.");
				} catch (SQLException e) {
					System.out.println("Wtf dropping");
					e.printStackTrace();
				}
			}
			return res;
		}
		
		public int updateCourse(String name, String teacher, int year){
			
			Statement myStmt;
			int res = 0;
			if(checkCourseExistance(name)){
				try {
					myStmt = myCon.createStatement();		
					//3. Execute a data manipulation query
					res = myStmt.executeUpdate("UPDATE `students`.`course` SET `teacher`='"+teacher+"', `study_year`='"+year+"' WHERE name='"+name+"';");
					System.out.println("New info for a course.");
				} catch (SQLException e) {
					System.out.println("Wtf dropping");
					e.printStackTrace();
				}
			}
			return res;
		}

		public String getCurrent_student_name() {
			return current_student_name;
		}

		public void setCurrent_student_name(String current_student_name) {
			this.current_student_name = current_student_name;
		}

		public String getCurrent_course_name() {
			return current_course_name;
		}

		public void setCurrent_course_name(String current_course_name) {
			this.current_course_name = current_course_name;
		}

		public String getCurrent_teacher_name() {
			return current_teacher_name;
		}

		public void setCurrent_teacher_name(String current_teacher_name) {
			this.current_teacher_name = current_teacher_name;
		}

		public String getCurrent_student_address() {
			return current_student_address;
		}

		public void setCurrent_student_address(String current_student_address) {
			this.current_student_address = current_student_address;
		}

		public String getCurrent_course_year() {
			return current_course_year;
		}

		public void setCurrent_course_year(String current_course_year) {
			this.current_course_year = current_course_year;
		}

}
