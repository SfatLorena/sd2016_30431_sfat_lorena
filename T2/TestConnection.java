package tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import connection.DBuse;

public class TestConnection {
	String URL = "jdbc:mysql://localhost:3306/students?autoReconnect=true&useSSL=false";
	String USER = "root";
	String PASS = "Moderator3*";
	DBuse newCon = new DBuse(URL,USER,PASS);
	
	//performed before each test method
	
	@Before
	public void setUp() throws Exception{
		newCon.connect();
		
		newCon.insertStudent("Tudor Amariei", "1990-10-01", "Bucuresti, Ilfov");
		newCon.insertStudent("Farcasanu Cristi", "1991-12-10", "Baia Mara");
		newCon.insertStudent("Pop Tiberiu", "1990-12-14", "Buzau");
		newCon.insertStudent("Brete Anamaria", "1992-10-03", "Zalau");
		
		newCon.insertCourse("Economy", "Tiberiu Parlea", 2);
		newCon.insertCourse("Programming Techniques", "Geta Dorin", 3);
	}
	
	@Test
	public void testConnect() {
		assertEquals(newCon.connect(),"connected");
	}
	
	
	@Test
	public void testInsertCourse(){
		assertEquals(newCon.checkCourseExistance("French"),true);
		//inserting a new user
		newCon.insertCourse("French", "Carla Breje", 3);
		//should say yes
		assertEquals(newCon.checkCourseExistance("French"),false);

	}
	
	@Test
	public void testDeleteCourse(){
		assertEquals(newCon.checkCourseExistance("French"),false);
		newCon.deleteCourseByName("French");
		//should say no
		assertEquals(newCon.checkCourseExistance("French"),true);
	}
	
	@Test 
	public void testInsertStudent(){
		
		newCon.simpleStudentQuery();
		newCon.deleteStudentByName("Gabriel Olosutean");
		assertEquals(newCon.checkStudentExistance("Gabriel Olosutean"),true);
		//inserting a new user
		newCon.insertStudent("Gabriel Olosutean", "1990-02-03", "Cluj, Baciu, nr 113");
		//should say yes
		assertEquals(newCon.checkStudentExistance("Gabriel Olosutean"),false);
		newCon.deleteStudentByName("Gabriel Olosutean");
		//should say no
		assertEquals(newCon.checkStudentExistance("Gabriel Olosutean"),true);

	}
	
	public void testDeleteStudent(){
		
		assertEquals(newCon.checkStudentExistance("Gabriel Olosutean"),true);
		newCon.insertStudent("Gabriel Olosutean", "1990-02-03", "Cluj, Baciu, nr 113");
		
		assertEquals(newCon.checkStudentExistance("Gabriel Olosutean"),false);
		//make sure to delete any possible items with the same name
		newCon.deleteStudentByName("Gabriel Olosutean");
		//should say no
		assertEquals(newCon.checkStudentExistance("Gabriel Olosutean"),true);

	}
	
	@Test
	public void testEnrollment(){
		
		assertEquals(newCon.checkEnrollment(6, 1),true);
		newCon.enrollClass(6, 1);
		assertEquals(newCon.checkEnrollment(6, 1),false);
	}
	
	@Test
	public void testDrop(){
		
		assertEquals(newCon.checkEnrollment(6, 1),false);
		newCon.dropClass(6, 1);
		assertEquals(newCon.checkEnrollment(6, 1),true);
	}
	
	@Test
	public void testStudentUpdate(){
		newCon.insertStudent("Pop Carmen", "1995-03-12", "Suceava");
		assertNotNull(newCon.updateStudent("Pop Carmen", "Badon, Salaj"));
		newCon.checkStudentExistance("Pop Carmen");
		//assertEquals(newCon.getCurrent_student_name(),"Pop Carmen");
		//assertEquals(newCon.getCurrent_student_address(),"Badon, Salaj");
		newCon.deleteStudentByName("Pop Carmen");
	}
	
	@Test
	public void testCourseUpdate(){
		newCon.insertCourse("Mathematics", "Breja Oana", 4);
		assertNotNull(newCon.updateCourse("Mathematics", "Pop Olga", 1));
		newCon.checkCourseExistance("Mathematics");
		//assertEquals(newCon.getCurrent_course_name(), "Mathematics");
		//assertEquals(newCon.getCurrent_teacher_name(),"Pop Olga");
		//assertEquals(newCon.getCurrent_course_year(),"1");
		newCon.deleteCourseByName("Mathematics");
	}
	
	@Test
	public void testQuery() {
		//newCon.simpleStudentQuery();
		//newCon.simpleCourseQuery();
		assertEquals(newCon.complexStudentQuery(" where name='Hosu Anda';"),"Hosu Anda 1995-01-01 Cluj-Napoca, Cluj"+"\n");
	}
}
