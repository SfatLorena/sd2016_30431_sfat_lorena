<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Display Users</title>
</head>
<body>
	<table id = "displayUsersTable" >
	<thead>
		<tr>
			<th>${username}</th>
			<th>${name}</th>
			<th>${surname}</th>
		</tr>
		</thead>
		<%-- <c:forEach var="user" items="${users}">
			<tr>
				<td>${user.username}</td>
				<td>${user.name}</td>
				<td>${user.surname}</td>
			</tr>
		</c:forEach> --%>
	</table>
	
	<button id="openPopupButton">Add user</button>
		<jsp:include page="../popup/addUserPopup.jsp" />

</body>
</html>