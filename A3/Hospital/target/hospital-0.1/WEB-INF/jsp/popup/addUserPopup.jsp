<div class="white-popup-block mfp-hide small-dialog zoom-anim-dialog"
	id="addUserPopupId">

	<div id="blackDiv">
		<h4 id="title-commentpopup">Adauga User</h4>

	</div>
	<div id="whiteDivDuplicatePopup">
		<br>

		<form method="POST" action="add" name="myForm" id="myForm">
			<label>${username}</label> <input type="text" name="username" /> <br>
			<label>${name}</label> <input type="text" name="name" /> <br> <label>${surname}</label>
			<input type="text" name="surname" /> <br>

		</form>

		<div class="row">
			<button id="addUserButton" type="button" class="btn btn-default">Adauga</button>
		</div>
	</div>
	<div id="responsemessage"></div>
</div>