$(document).ready(function() {
	$("#openPopupButton").click(function() {
		var popupExtraConfig = {
			     callbacks : {
			      afterClose : function() {
			    	  openPopupWithMessage("#success-modal",null, "bpooom");
			      }
			     }
			    };
		openPopup("#addUserPopupId",popupExtraConfig);
		
});
	
	$("#addUserButton").on("click",function(e) {
		e.preventDefault();
		var myForm = $("#myForm");
		var view = formDataToJson(myForm);
        ajaxCallSubmitAddUserForm(view);
	
	});
	
	
	
}
)