function openPopup(popupId, popupExtraConfig) {
	
	$(popupId).removeAttr('style'); 
	if ( ! $(popupId).hasClass('zoom-anim-dialog')) {
		 $(popupId).addClass('zoom-anim-dialog');
	}
	
	$(popupId).children().first().css('cursor', 'move');
	
	var popupConfig = {
		removalDelay : 70,
		items : {
			src : popupId
		},
		mainClass : 'my-mfp-slide-bottom',
		type : 'inline'
	};
	
	$.extend(popupConfig, popupExtraConfig);
	$.magnificPopup.open(popupConfig);
	$('.mfp-close').addClass('mfp-close-white');
	
	setTimeout(function(){
		$(popupId).removeClass('zoom-anim-dialog');
	                     },
		700
    );
}

function openPopupWithMessage(popupId, popupExtraConfig, message) {
	
	openPopup(popupId, popupExtraConfig);
	$("#padding-info-popup").text(message);
}

function formDataToJson(formElement) {
	 
	 var $this = formElement
	  , viewArr = $this.serializeArray()
	  , view = {};

	 for (var i in viewArr) {
	  view[viewArr[i].name] = viewArr[i].value;
	 }
	  
	  return view;   
	}