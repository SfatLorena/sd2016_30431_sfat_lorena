package org.hospital.services;

import java.util.List;

import org.hospital.dao.PatientDAO;
import org.hospital.entities.Patient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("patientService")
public class PatientService {

	@Autowired
	PatientDAO patientDao;
	
	
	@Transactional
	public Patient createPatient(int patientId){
		return patientDao.getPatientById(patientId);
	}
	@Transactional
	public Patient getPatientById(int patientId){
		return patientDao.getPatientById(patientId);
	}
	@Transactional
	public List<Patient> getAllPatients(int patientId){
		return patientDao.getAllPatients();
	}
	@Transactional
	public void deletePatient(int patientId){
		patientDao.deletePatient(patientId);
	}
	@Transactional
	public void updatePatient(int patientId){
		patientDao.updatePatient(patientId);
	}
	
	
}
