package org.hospital.services;

import org.hospital.dao.ConsultationDAO;
import org.hospital.dao.PatientDAO;
import org.hospital.entities.Consultation;
import org.hospital.entities.Patient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("consultationService")
public class ConsultationService {
	@Autowired
	ConsultationDAO consultationDao;
	@Autowired
	PatientService patientService;
	
	@Transactional
	public Consultation getConsultationById(int consultationId){
		Consultation consultation = consultationDao.getConsultationById(consultationId);
		return consultation;
	}
	
	@Transactional
	public Consultation getConsultationByPatientId(int patientId){
		Patient patient = patientService.getPatientById(patientId);
		Consultation consultation = consultationDao.getConsultationByPatient(patient);
		return consultation;
	}
	
	@Transactional
	public void deleteConsultation(int consultationId){
		Consultation consultation = getConsultationById(consultationId);
		consultationDao.deleteConsultation(consultation);
	}
	
	@Transactional
	public void updateConsultation(int consultationId){
		Consultation consultation = getConsultationById(consultationId);
		consultationDao.updateConsultation(consultation);
	}
}
