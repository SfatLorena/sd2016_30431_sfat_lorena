package org.hospital.services;

import org.hospital.dao.UserDAO;
import org.hospital.entities.User;
import org.hospital.form.UserForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service("userService")
public class UserService implements UserDetailsService {
	
	@Autowired
	private UserDAO userDAO;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userDAO.getUserByUsername(username);
		return user;
	}
	
	public void updateUser(UserForm userForm, String username){
		User user = userDAO.getUserByUsername(username);
		if(user == null)
			return;
		//TODO convert from UserForm to User
	}
	public void deleteUser(UserForm userForm, String username){
		User user = userDAO.getUserByUsername(username);
		userDAO.deleteUser(user);
	}
	public void createUser(UserForm userForm, String username){
		User user = userDAO.getUserByUsername(username);
		if(user == null)
			return;
		//TODO convert from UserForm to User
	}
}
