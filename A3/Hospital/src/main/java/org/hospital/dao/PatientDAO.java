package org.hospital.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.hospital.entities.Patient;
import org.springframework.stereotype.Repository;

@Repository("patientDao")
public class PatientDAO {
	private EntityManager entityManager;
	
	public Patient createPatient(Patient patient){
		entityManager.persist(patient);
		return patient;
	}
	
	public Patient getPatientById(int patientId){
		return entityManager.find(Patient.class, patientId);
	}
	
	public List<Patient> getAllPatients(){
		String sqlStatement = "SELECT e FROM Patient e";
		Query query = entityManager.createQuery(sqlStatement, Patient.class);
		try {
			List<Patient> events = query.getResultList();
			return events;
		} catch (NoResultException e) {
			return null;
		}
	}
	
	public void deletePatient(int patientId){
		Patient patient = getPatientById(patientId);
		entityManager.remove(patient);
	}
	
	public void updatePatient(int patientId){
		Patient patient = getPatientById(patientId);
		entityManager.merge(patient);
	}
}
