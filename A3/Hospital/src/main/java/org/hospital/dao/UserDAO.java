package org.hospital.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hospital.entities.User;
import org.springframework.stereotype.Service;

@Service("userDao")
public class UserDAO {

	@PersistenceContext
	private EntityManager entityManager;
	
	public User getUserByUsername(String username) {
		return (User) entityManager.createQuery("SELECT u FROM User u WHERE u.username = :username").setParameter("username", username).getResultList().get(0);
	}
	
	public void deleteUser(User user){
		entityManager.remove(user);
	}
	
	public User createUser(User user){
		entityManager.persist(user);
		return user;
	}
	
	public void updateUser(User user){
		entityManager.merge(user);
	}
}
