package org.hospital.dao;

import javax.persistence.EntityManager;

import org.hospital.entities.Consultation;
import org.hospital.entities.Patient;
import org.springframework.stereotype.Repository;

@Repository("consultationDao")
public class ConsultationDAO {
	private EntityManager entityManager;
	
	public Consultation createConsultation(Consultation consultation){
		entityManager.persist(consultation);
		return consultation;
	}
	public Consultation getConsultationById(int consultationId){
		return entityManager.find(Consultation.class, consultationId);
	}
	public Consultation getConsultationByPatient(Patient patient){
		return entityManager.find(Consultation.class, patient);
	}
	public void deleteConsultation(Consultation consultation){
		entityManager.remove(consultation);
	}
	public void updateConsultation(Consultation consultation){
		entityManager.merge(consultation);
	}
}
