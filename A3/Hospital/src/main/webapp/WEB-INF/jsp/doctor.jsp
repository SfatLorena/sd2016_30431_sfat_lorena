<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<style type="text/css">
    <%@include file="../../resources/css/3rdParty/bootstrap.css" %>
</style>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<script src="/js/3rdParty/bootstrap.min.js"></script>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
</head>
<body>
<h1>Doctor page</h1>
<c:if test="${pageContext.request.userPrincipal.name != null}">
	<sec:authorize access="hasRole('ROLE_DOCTOR')">
		   <h2>Welcome : ${pageContext.request.userPrincipal.name} <c:url value="/logout" var="logoutUrl" />
			<a href="${logoutUrl}">Log Out</a></h2>     		
	<button type="button" class="btn btn-info">Add Patient!</button>
	<button type="button" class="btn btn-info">Update Patient!</button>
	</sec:authorize>
</c:if>

</body>
</html>