<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>

<html>
<body>

<h1>Administrator page</h1>
<br>
	<c:if test="${pageContext.request.userPrincipal.name != null}">
	   <h2>Welcome : ${pageContext.request.userPrincipal.name}</h2>  
		<c:url value="/logout" var="logoutUrl" />
		<a href="${logoutUrl}">Log Out</a>
	</c:if>
	
	
	<sec:authorize access="hasRole('ROLE_ADMIN')">

		<h1>ADMIN</h1>

	</sec:authorize>
</body>
</html>