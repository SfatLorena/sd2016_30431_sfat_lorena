$(document).ready(function() {
	table = $('#displayUsersTable').DataTable( {
		ajax : serverPath + '/getAllUsers',
		  "columns" : [ {
			   "data" : "username"
			  }, {
			   "data" : "name"
			  }, {
			   "data" : "surname"
			  } ]
    } );
})


function ajaxCallSubmitAddUserForm(data, callUrl) {

 $.ajax({
  url : serverPath + '/addUserAjax',
  data : JSON.stringify(data),
  type : "POST",
  contentType : 'application/json; charset=utf-8',
  error : function(jqXHR, textStatus, errorThrown) {
   console.log(textStatus, errorThrown);
  },
  success : function(data) {

	  var hasAccess = Object.keys(data)[0];
	  var message = data[hasAccess];
	   if (hasAccess === "true") {
		//    openPopupWithMessage("#success-modal", message);
		   $("#responsemessage").text(message); 
		   $("#whiteDivDuplicatePopup").hide();
	  table.ajax.reload();
	   } else {
		//  openPopupWithMessage("#error-modal", message);
		   $("#responsemessage").text(message);
		   $("#whiteDivDuplicatePopup").hide();
		   }
	 
  }
 });

}

