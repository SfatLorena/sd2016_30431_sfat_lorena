package org.lavender.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "product")
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idproduct")
	private int id;
	
	@Column(name = "name", nullable = false)
	private String name;
	
	@Column(name = "amount", nullable = false)
	private int amount;
	
	@Column(name = "description", nullable = false)
	private String desctiption;
}
