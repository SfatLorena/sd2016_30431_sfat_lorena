package org.lavender.controllers;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class WelcomeController {


	@RequestMapping(value = "/welcomeSummer", method = RequestMethod.GET)
	public void welcome2(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.setStatus(HttpServletResponse.SC_OK);
		response.getWriter().write("Summer is here, " + request.getParameter("name"));
	}

	@RequestMapping(value = "/welcomeSummerAgain", method = RequestMethod.GET)
	public void welcome3(@RequestParam(value = "name") String name, HttpServletResponse response) throws IOException {
		response.setStatus(HttpServletResponse.SC_OK);
		response.getWriter().write("Summer is here, " + name);
	}

	@RequestMapping(value = "/welcomeJsp", method = RequestMethod.GET)
	public String welcome4(@RequestParam(value = "name", required = false) String name) throws IOException {
		return "welcome";
	}

	@RequestMapping(value = "/welcomeJspAgain", method = RequestMethod.GET)
	public ModelAndView welcome5() throws IOException {

		return new ModelAndView("welcome");
	}

	@RequestMapping(value = "/welcomeJspWithModelAndView", method = RequestMethod.GET)
	public ModelAndView welcome6() throws IOException {

		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("welcome");
		modelAndView.addObject("something", "Gusterule");
		return modelAndView;
	}

	@RequestMapping(value = "/welcomeJspWithModel", method = RequestMethod.GET)
	public String welcome7(Model model) throws IOException {

		model.addAttribute("something", "Toader");
		return "firstExample";
	}

}
