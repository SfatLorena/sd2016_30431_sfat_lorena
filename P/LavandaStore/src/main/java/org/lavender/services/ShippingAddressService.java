package org.lavender.services;

import org.lavender.dao.ShippingAddressDao;
import org.lavender.entities.ShippingAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("shippingAddressService")
public class ShippingAddressService {
	@Autowired
	ShippingAddressDao shippingAddressDao;

	@Transactional
	public ShippingAddress getShippingAddressById(int shippingAddressId) {
		ShippingAddress shippingAddress = shippingAddressDao.getShippingAddressById(shippingAddressId);
		return shippingAddress;
	}

	@Transactional
	public void deleteShippingAddress(int shippingAddressId) {
		ShippingAddress shippingAddress = getShippingAddressById(shippingAddressId);
		shippingAddressDao.deleteShippingAddress(shippingAddress);
	}

	@Transactional
	public void updateShippingAddress(int shippingAddressId) {
		ShippingAddress shippingAddress = getShippingAddressById(shippingAddressId);
		shippingAddressDao.updateShippingAddress(shippingAddress);
	}
}
