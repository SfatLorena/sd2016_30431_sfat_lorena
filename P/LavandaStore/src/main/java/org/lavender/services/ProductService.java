package org.lavender.services;

import java.util.List;

import org.lavender.dao.ProductDAO;
import org.lavender.entities.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("productService")
public class ProductService {
	@Autowired
	ProductDAO productDao;

	@Transactional
	public Product getProductById(int productId) {
		Product product = productDao.getProductById(productId);
		return product;
	}

	@Transactional
	public List<Product> getAllProducts() {
		return productDao.getAllProducts();
	}

	@Transactional
	public void deleteProduct(int productId) {
		Product product = getProductById(productId);
		productDao.deleteProduct(product);
	}

	@Transactional
	public void updateProduct(int productId) {
		Product product = getProductById(productId);
		productDao.updateProduct(product);
	}
}
