package org.lavender.services;

import java.util.List;

import org.lavender.dao.OrderDAO;
import org.lavender.entities.Order;
import org.lavender.form.OrderForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("orderService")
public class OrderService {

	@Autowired
	OrderDAO orderDao;
	
	
	@Transactional
	public Order createOrder(int orderId){
		return orderDao.getOrderById(orderId);
	}
	@Transactional
	public Order getOrderById(int orderId){
		return orderDao.getOrderById(orderId);
	}
	@Transactional
	public List<Order> getAllOrders(int orderId){
		return orderDao.getAllOrders();
	}
	@Transactional
	public void deleteOrder(int orderId){
		orderDao.deleteOrder(orderId);
	}
	@Transactional
	public void updateOrder(OrderForm orderForm, int orderId){
		Order order = orderDao.getOrderById(orderId);
		if(orderForm != null){
			convertOrderFormToOrder(orderForm,order);
		}
	}
	
	private void convertOrderFormToOrder(OrderForm orderForm, Order order){
		//TODO
	}
}
