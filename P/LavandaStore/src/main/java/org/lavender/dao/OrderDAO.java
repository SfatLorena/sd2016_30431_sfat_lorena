package org.lavender.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.lavender.entities.Order;
import org.lavender.entities.Order;
import org.springframework.stereotype.Repository;

@Repository("orderDao")
public class OrderDAO {
	private EntityManager entityManager;
	
	public Order createOrder(Order order){
		entityManager.persist(order);
		return order;
	}
	
	public Order getOrderById(int orderId){
		return entityManager.find(Order.class, orderId);
	}
	
	public List<Order> getAllOrders(){
		String sqlStatement = "SELECT e FROM Order e";
		Query query = entityManager.createQuery(sqlStatement, Order.class);
		try {
			List<Order> events = query.getResultList();
			return events;
		} catch (NoResultException e) {
			return null;
		}
	}
	
	public void deleteOrder(int orderId){
		Order order = getOrderById(orderId);
		entityManager.remove(order);
	}
	
	public void updateOrder(Order order){
		entityManager.merge(order);
	}
}
