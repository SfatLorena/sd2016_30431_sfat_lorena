package org.lavender.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.lavender.entities.Product;
import org.lavender.entities.ShippingAddress;
import org.springframework.stereotype.Repository;

@Repository("productDao")
public class ProductDAO {
	private EntityManager entityManager;

	public Product createProduct(Product product) {
		entityManager.persist(product);
		return product;
	}

	public Product getProductById(int productId) {
		return entityManager.find(Product.class, productId);
	}

	public void deleteProduct(Product product) {
		entityManager.remove(product);
	}

	public void updateProduct(Product product) {
		entityManager.merge(product);
	}
	
	public List<Product> getAllProducts(){
		String sqlStatement = "SELECT e FROM Product e";
		Query query = entityManager.createQuery(sqlStatement, ShippingAddress.class);
		try {
			List<Product> products = query.getResultList();
			return products;
		} catch (NoResultException e) {
			return null;
		}
	}
}
