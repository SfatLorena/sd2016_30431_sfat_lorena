package org.lavender.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.lavender.entities.ShippingAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("patientDao")
public class ShippingAddressDao {
	
	@Autowired
	private EntityManager entityManager;

	public ShippingAddress createShippingAddress(ShippingAddress shippingAddress) {
		entityManager.persist(shippingAddress);
		return shippingAddress;
	}

	public ShippingAddress getShippingAddressById(int shippingAddressId) {
		return entityManager.find(ShippingAddress.class, shippingAddressId);
	}

	public List<ShippingAddress> getAllShippingAddresss() {
		String sqlStatement = "SELECT e FROM ShippingAddress e";
		Query query = entityManager.createQuery(sqlStatement, ShippingAddress.class);
		try {
			List<ShippingAddress> events = query.getResultList();
			return events;
		} catch (NoResultException e) {
			return null;
		}
	}

	public void deleteShippingAddress(ShippingAddress shippingAddress) {
		entityManager.remove(shippingAddress);
	}

	public void updateShippingAddress(ShippingAddress shippingAddress) {
		entityManager.merge(shippingAddress);
	}
}
