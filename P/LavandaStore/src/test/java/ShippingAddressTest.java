import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.lavender.dao.ShippingAddressDao;
import org.lavender.entities.ShippingAddress;
import org.springframework.beans.factory.annotation.Autowired;

public class ShippingAddressTest {

	ShippingAddress addr1;
	ShippingAddress addr2;


	ShippingAddressDao addrDao = new ShippingAddressDao();

	@Before
	public void setUp() {
		addr1 = new ShippingAddress();
		addr1.setCity("Zalau");
		addr1.setCounty("Salaj");
		addr1.setNumber(11);
		addr1.setPostalCode("1234");

		addrDao.createShippingAddress(addr1);

		addr2 = new ShippingAddress();
		addr2.setCity("Badon");
		addr2.setCounty("Salaj");
		addr2.setNumber(22);
		addr2.setPostalCode("5678");
	}

	@After
	public void cleanUp() {
		addrDao.createShippingAddress(addr1);

		addr2.setPostalCode("5678");
	}

	@Test
	public void add() {
		ShippingAddress sadr = new ShippingAddress();
		sadr.setCity("test");
		sadr.setCounty("test");
		sadr.setNumber(1);
		sadr.setPostalCode("2132");
	}

}
