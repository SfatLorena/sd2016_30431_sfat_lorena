<div id="error-modal" class="white-popup-block mfp-hide small-dialog zoom-anim-dialog">

	<div id="blackDiv">
		<h4 id="title-commentpopup">Error</h4>
	</div>
	<div class="row">
		<div class="col-md-2">
			<i id="error-popup-red" class="fa fa-minus-circle fa-4x"></i>
		</div>

		<div class="col-md-10">
			<div id="padding-info-popup"></div>
		</div>

	</div>
	
</div>

<div id="warning-modal" class="white-popup-block mfp-hide small-dialog zoom-anim-dialog">

	<div id="blackDiv">
		<h4 id="title-commentpopup">Warning</h4>
	</div>
	<div class="row">
		<div class="col-md-2">
			<i id="warning-popup-yellow" class="fa fa-exclamation-triangle fa-4x"></i>
		</div>

		<div class="col-md-10">
			<div id="padding-info-popup"></div>
		</div>

	</div>
	
</div>

<div id="success-modal" class="white-popup-block mfp-hide small-dialog zoom-anim-dialog">

	<div id="blackDiv">
		<h4 id="title-commentpopup">Success</h4>
	</div>
	<div class="row">
		<div class="col-md-2">
			<i id="warning-popup-yellow" class="fa fa-exclamation-triangle fa-4x"></i>
		</div>

		<div class="col-md-10">
			<div id="padding-info-popup"></div>
		</div>

	</div>
	
</div>
