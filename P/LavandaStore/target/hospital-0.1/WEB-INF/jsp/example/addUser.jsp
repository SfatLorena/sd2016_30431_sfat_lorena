<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add user</title>

<script type="text/javascript">
 function addUser() {
  document.getElementsByName('name')[0].value = "Alina";
  var lForm = document.forms['myForm'];
  lForm.action = "add";
  lForm.submit();
 }
 
</script>

</head>
<body>
	<form method="POST" action="add" modelAttribute="userForm" name="myForm">
		<label>${username}</label> <input type="text" name = "username"/> <br>
		<label>${name}</label> <input type="text" name = "name"/> <br>
		<label>${surname}</label> <input type="text" name = "surname"/> <br>
		<input type="submit" onclick="addUser()" value="Add user" />
	</form>
	
	
</body>
</html>