package presentation.layer;

import java.awt.EventQueue;

public class Main {

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AuthenticationScreen frame = new AuthenticationScreen();
					//EmployeeScreen frame = new EmployeeScreen();
					//AdministratorScreen frame = new AdministratorScreen();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
