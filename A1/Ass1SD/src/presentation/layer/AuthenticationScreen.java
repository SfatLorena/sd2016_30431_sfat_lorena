package presentation.layer;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import business.layer.models.Administrator;
import business.layer.models.Employee;
import business.layer.service.AdministratorService;
import data.source.dao.AdministratorDAO;
import data.source.dao.EmployeeDAO;

import javax.swing.JRadioButton;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.JPasswordField;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ButtonGroup;

public class AuthenticationScreen extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textField;
	private JPasswordField passwordField;
	private final ButtonGroup buttonGroup = new ButtonGroup();



	/**
	 * Create the frame.
	 */
	public AuthenticationScreen() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setTitle("Log In");
		
		final JRadioButton employee = new JRadioButton("employee");
		buttonGroup.add(employee);
		employee.setBounds(160, 158, 121, 42);
		employee.setSelected(true);
		contentPane.add(employee);
		
		textField = new JTextField();
		textField.setBounds(147, 22, 260, 31);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JRadioButton administrator = new JRadioButton("administrator");
		buttonGroup.add(administrator);
		administrator.setBounds(287, 158, 141, 42);
		contentPane.add(administrator);
				
		JTextArea txtrUsername = new JTextArea();
		txtrUsername.setText("Username");
		txtrUsername.setBounds(10, 25, 127, 28);
		contentPane.add(txtrUsername);
		
		JTextPane txtpnPassword = new JTextPane();
		txtpnPassword.setText("Password");
		txtpnPassword.setBounds(10, 83, 127, 31);
		contentPane.add(txtpnPassword);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(147, 83, 260, 31);
		contentPane.add(passwordField);
		
		JTextArea txtrLoginAs = new JTextArea();
		txtrLoginAs.setText("Login as:");
		txtrLoginAs.setBounds(10, 167, 127, 22);
		contentPane.add(txtrLoginAs);
		
		JButton loginButton = new JButton("LogIn");
		loginButton.setBounds(166, 220, 121, 31);
		loginButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String username = textField.getText();
				String password = String.valueOf(passwordField.getPassword());
				Administrator admin;
				final Employee empl;
				
				if(checkEmpty(username) || checkEmpty(password)){
					popupError("Fields must not be empty!");
				}
				else
				{
					//Look it up in DB
					if(employee.isSelected()){
						//search the employee
						
						EmployeeDAO employeeD = new EmployeeDAO();
						if(!employeeD.searchByKey(username)){
							popupError("Wrong Username");
						}
						else{
							empl = employeeD.getByKey(username);
							System.out.println("Trying to log in: "+empl);
							if(!empl.getPassword().equals(password)){
								System.out.println("PASSWORD missmatch! "+password+" VS "+empl.getPassword());
								popupError("Wrong password!");
							}
							else{
								System.out.println(empl+" logged in!");
								
								EventQueue.invokeLater(new Runnable() {
									public void run() {
										try {
											EmployeeScreen frame = new EmployeeScreen(empl);
											frame.setVisible(true);
										} catch (Exception e) {
											e.printStackTrace();
										}
									}
								});
								
								dispose();
							}
						}
					}
					else{
						//search the admin
						AdministratorDAO adminD = new AdministratorDAO();
						if(!adminD.searchByKey(username)){
							popupError("Wrong Username");
						}
						else{
							admin = adminD.getbyKey(username);
							System.out.println("Trying to log in: "+admin);
							if(!admin.getPassword().equals(password)){
								System.out.println("PASSWORD missmatch! "+password+" VS "+admin.getPassword());
								popupError("Wrong password!");
							}
							else{
								System.out.println(admin+" logged in!");
								
								EventQueue.invokeLater(new Runnable() {
									public void run() {
										try {
											AdministratorScreen frame = new AdministratorScreen();
											frame.setVisible(true);
										} catch (Exception e) {
											e.printStackTrace();
										}
									}
								});
								
								dispose();
							}
								
						}
					}
				}
			}
		});
		contentPane.add(loginButton);
	}

	
	private boolean checkEmpty(String str){
		if(str.equals(""))
			return true;
		else
			return false;
	}
	
	private void popupError(String str){
		JOptionPane.showMessageDialog(null, str, "Login Error",
                JOptionPane.ERROR_MESSAGE);
	}
}
