package presentation.layer;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.table.DefaultTableModel;

import business.layer.models.Client;
import business.layer.models.Employee;
import data.source.dao.ClientDAO;
import data.source.dao.EmployeeDAO;

import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;

public class DisplayClients extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private final DefaultTableModel model = new DefaultTableModel();

	/**
	 * Create the frame.
	 */
	public DisplayClients() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 383);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		setTitle("Clients Table");
		
		table = new JTable(model);
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Nume", "Prenume", "CNP", "Address"
			}
		));
		
		ClientDAO ed = new ClientDAO();
		ArrayList<Client> employees = (ArrayList<Client>) ed.view();
		
		int numCols = table.getModel().getColumnCount();
		
		for(int i = 0; i<employees.size(); i++){
			Object [] fila = new Object[numCols];
			fila[0] = employees.get(i).getName();
			fila[1] = employees.get(i).getSurname();
			fila[2] = employees.get(i).getCNP();
			fila[3] = employees.get(i).getAddress();
					
			((DefaultTableModel) table.getModel()).addRow(fila);
		}	
		
		JScrollPane scrollPane = new JScrollPane();
		
		JButton btnDone = new JButton("Done");
		btnDone.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addContainerGap()
							.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 405, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(181)
							.addComponent(btnDone)))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 282, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(btnDone)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		
		
		scrollPane.setViewportView(table);
		contentPane.setLayout(gl_contentPane);
	}

}
