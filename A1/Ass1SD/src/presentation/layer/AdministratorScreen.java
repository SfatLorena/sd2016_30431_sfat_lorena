package presentation.layer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.awt.event.ActionEvent;
import javax.swing.table.DefaultTableModel;

import business.layer.models.Activity;
import business.layer.models.Employee;
import business.layer.service.AdministratorService;
import data.source.dao.EmployeeDAO;

public class AdministratorScreen extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable table;
	private AdministratorService adminServ = new AdministratorService();
	private static int raportNR = 1;

	/**
	 * Create the frame.
	 */
	public AdministratorScreen() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 321);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setTitle("Administrator Menu");
		
		setContentPane(contentPane);
		
		JScrollPane scrollPane = new JScrollPane();
		
		final DefaultTableModel model = new DefaultTableModel();
		
		JButton btnViewEmployees = new JButton("View Employees");
		btnViewEmployees.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				//TODO refresh
				
				//"Username", "Name", "Surname", "Address"
				
				clearTable(model);
				
				EmployeeDAO ed = new EmployeeDAO();
				ArrayList<Employee> employees = (ArrayList<Employee>) ed.view();
				
				int numCols = table.getModel().getColumnCount();
				
				for(int i = 0; i<employees.size(); i++){
					Object [] fila = new Object[numCols]; 
					fila[0] = employees.get(i).getUsername();
					fila[1] = employees.get(i).getName();
					fila[2] = employees.get(i).getSurname();
					fila[3] = employees.get(i).getAddress();
							
					((DefaultTableModel) table.getModel()).addRow(fila);
				}							
			}
		});
		
		JButton btnAddEmployee = new JButton("Add Employee");
		btnAddEmployee.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				JTextField name = new JTextField();
				JTextField surname = new JTextField();
				JTextField username = new JTextField();
				JTextField password = new JTextField();
				JTextField address = new JTextField();
				JTextField salary = new JTextField();
				final JComponent[] inputs = new JComponent[] {
						new JLabel("Username"),
						username,
						new JLabel("Password"),
						password,
						new JLabel("Name"),
						name,
						new JLabel("Surname"),
						surname,
						new JLabel("Address"),
						address,
						new JLabel("Salary"),
						salary
				};
				JOptionPane.showMessageDialog(null, inputs, "New Employee Info", JOptionPane.PLAIN_MESSAGE);
				System.out.println("New employee info: " +
						name.getText() + ", " +
						surname.getText()+", "+address.getText());
				
				String nameStr = name.getText();
				String surnameStr = surname.getText();
				String usernameStr = username.getText();
				String passwordStr = password.getText();
				String addressStr = address.getText();
				double salaryStr;
				if(checkEmpty(salary.getText()))
					salaryStr = 0;
				else
					salaryStr =Double.parseDouble(salary.getText());
				
				if(checkEmpty(nameStr) || checkEmpty(surnameStr) || checkEmpty(usernameStr) 
						|| checkEmpty(passwordStr) || checkEmpty(addressStr) || salaryStr == 0){
					popupError("Fields must not be empty!");
				}
				else if(!checkName(name.getText()) || !checkName(surname.getText()))
					popupError("Names contain only letters!");
				else if(!checkNumber(salary.getText()))
					popupError("Salary must me a number!");
				else{
					if(!adminServ.getEd().searchByKey(usernameStr) && !adminServ.getAdmind().searchByKey(usernameStr)){
						//Good to go!
						if(!adminServ.addEmployee(usernameStr, passwordStr, nameStr, surnameStr, addressStr, salaryStr)){
							popupError("Failed to add employee! Password not strong enough!");
						}
						else popupSuccess("Successfully added a new employee!");
					}
					else
						popupError("Username already taken!");
				}
			}
		});
		
		JButton btnUpdateEmployee = new JButton("Update Employee");
		btnUpdateEmployee.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				JTextField username = new JTextField();
				JTextField name = new JTextField();
				JTextField surname = new JTextField();
				JTextField address = new JTextField();
				JTextField salary = new JTextField();
				final JComponent[] inputs = new JComponent[] {
						new JLabel("Select employee username"),
						username,
						new JLabel("New name"),
						name,
						new JLabel("New Surname"),
						surname,
						new JLabel("New Address"),
						address,
						new JLabel("New Salary"),
						salary,
						new JLabel("*Fields left empty will remain unchanged. ")
				};
				JOptionPane.showMessageDialog(null, inputs, "Updated Employee Info", JOptionPane.PLAIN_MESSAGE);
				System.out.println("Updated employee info: " +
						name.getText() + ", " +
						surname.getText()+", "+address.getText()+", "+salary.getText());
				if(username.equals("") && name.equals("") && surname.equals("") && address.equals("")){
					popupError("No changes made, no need for an update!");
				}
				else if(!checkName(name.getText()) || !checkName(surname.getText()))
					popupError("Names contain only letters!");
				else if(!checkNumber(salary.getText()))
					popupError("Salary must me a number!");
				else{
					adminServ.updateEmployee(username.getText(), name.getText(), surname.getText(), address.getText(), Double.parseDouble(salary.getText()));
					popupSuccess("Successfully epdated an employee!");
				}
			}
		});
		
		JButton btnDeleteEmployee = new JButton("Delete Employee");
		btnDeleteEmployee.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JTextField username = new JTextField();
				final JComponent[] inputs = new JComponent[] {
						new JLabel("Username"),
						username
				};
				JOptionPane.showMessageDialog(null, inputs, "Delete Employee", JOptionPane.PLAIN_MESSAGE);
				System.out.println("Deleted employee: "+username.getText());
				if(username.getText().equals("")){
					popupError("Select an employee to delete!");
				}
				else{
					if(!adminServ.deleteEmployee(username.getText()))
						popupError("Failed to delete the employee!");
					else
						popupSuccess("Successfully deleted and employee!");
				}
			}
		});
		
		JButton btnGenerateReport = new JButton("Generate Report");
		btnGenerateReport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				PrintWriter writer = null;
				try {
					writer = new PrintWriter("report-"+raportNR+".txt", "UTF-8");
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (UnsupportedEncodingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				raportNR++;
				
				JTextField username = new JTextField();
				final JComponent[] inputs = new JComponent[] {
						new JLabel("Username"),
						username
				};
				JOptionPane.showMessageDialog(null, inputs, "Select Employee", JOptionPane.PLAIN_MESSAGE);
				
				if(checkEmpty(username.getText()))
					popupError("Field must not be empty!");
				else{
					
					if(!adminServ.getEd().searchByKey(username.getText()))
						popupError("No such employee!");
					else{
						Employee employee = adminServ.getEd().getByKey(username.getText());
						ArrayList<Activity> activity = (ArrayList<Activity>) adminServ.viewReport(employee);
						
						for(int i = 0; i<activity.size(); i++){
							writer.println(activity.get(i).toString());
						}
						
						writer.close();
					}
				}
			}
		});
		
		JButton btnDone = new JButton("Done");
		btnDone.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		
		JButton btnNewAdmin = new JButton("New Admin");
		btnNewAdmin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				JTextField username = new JTextField();
				JTextField name = new JTextField();
				JTextField surname = new JTextField();
				JTextField address = new JTextField();
				JTextField password = new JTextField();
				final JComponent[] inputs = new JComponent[] {
						new JLabel("Select username"),
						username,
						new JLabel("Select Password"),
						password,
						new JLabel("Name"),
						name,
						new JLabel("Surname"),
						surname,
						new JLabel("Address"),
						address						
				};
				JOptionPane.showMessageDialog(null, inputs, "Updated Employee Info", JOptionPane.PLAIN_MESSAGE);
				System.out.println("New Admin info: " +
						name.getText() + ", " +
						surname.getText()+", "+address.getText());
				if(checkEmpty(username.getText()) || checkEmpty(password.getText()) || 
						checkEmpty(name.getText()) || checkEmpty(surname.getText()) || checkEmpty(address.getText())){
					popupError("All fields are mandatory!");
				}
				else if(!checkName(name.getText()) || !checkName(surname.getText()))
					popupError("Names contain only letters!");
				else{
					if(!adminServ.addAdmin(username.getText(), password.getText(),
							name.getText(), surname.getText(), address.getText()))
						popupError("Failed to add new Admin!");
					else
						popupSuccess("Successfully added a new Admin!");
				}
			}
		});
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addContainerGap()
									.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
										.addComponent(btnGenerateReport, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addComponent(btnDeleteEmployee, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addComponent(btnViewEmployees, GroupLayout.PREFERRED_SIZE, 131, GroupLayout.PREFERRED_SIZE))
									.addPreferredGap(ComponentPlacement.RELATED))
								.addGroup(gl_contentPane.createSequentialGroup()
									.addContainerGap()
									.addComponent(btnAddEmployee, GroupLayout.DEFAULT_SIZE, 133, Short.MAX_VALUE))
								.addGroup(gl_contentPane.createSequentialGroup()
									.addContainerGap()
									.addComponent(btnUpdateEmployee, GroupLayout.PREFERRED_SIZE, 133, Short.MAX_VALUE)))
							.addGroup(gl_contentPane.createSequentialGroup()
								.addContainerGap()
								.addComponent(btnNewAdmin, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)))
						.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
							.addContainerGap()
							.addComponent(btnDone)
							.addGap(35)))
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 281, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 239, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(btnViewEmployees)
							.addGap(8)
							.addComponent(btnAddEmployee)
							.addGap(14)
							.addComponent(btnUpdateEmployee)
							.addGap(19)
							.addComponent(btnDeleteEmployee)
							.addGap(15)
							.addComponent(btnGenerateReport)
							.addGap(10)
							.addComponent(btnNewAdmin)
							.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addComponent(btnDone)))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		
		table = new JTable(model);
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Username", "Name", "Surname", "Address"
			}
		));
		
		
		scrollPane.setViewportView(table);
		contentPane.setLayout(gl_contentPane);
	}
	
	private boolean checkEmpty(String str){
		if(str.equals(""))
			return true;
		else
			return false;
	}
	
	private void popupError(String str){
		JOptionPane.showMessageDialog(null, str, "Error",
                JOptionPane.ERROR_MESSAGE);
	}
	
	private void popupSuccess(String str){
		JOptionPane.showMessageDialog(null, str, "Done",
                JOptionPane.PLAIN_MESSAGE);
	}
	
	public static void clearTable(final DefaultTableModel model) {
		
		 while (model.getRowCount() > 0){
		        for (int i = 0; i < model.getRowCount(); ++i){
		            model.removeRow(i);
		        }
		    }
	}
		
	
	private boolean checkNumber(String str){
		for (int i = 0; i < str.length(); i++) {
		      if (!Character.isDigit(str.charAt(i)))
		        return false;
		    }
		    return true;
	}
	
	private boolean checkName(String str){
		for (int i = 0; i < str.length(); i++) {
		      if (Character.isDigit(str.charAt(i)))
		        return false;
		    }
		    return true;
	}
}
