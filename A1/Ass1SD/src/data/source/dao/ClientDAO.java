package data.source.dao;


import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import business.layer.models.Account;
import business.layer.models.Administrator;
import business.layer.models.Client;

public class ClientDAO extends SessionImpl{
	
	public boolean add(Client entity){

		if(okToAdd(entity)){
			openSession();
			System.out.println("Session Opened for ADD.");
			 	session.save(entity);
			 	System.out.println("Session Closed for ADD.");
			closeSession();
			return true;
		}
		return false;
	}
	
	public boolean delete(Client entity){

		if(okToDelete(entity)){
			openSession();
			
			 	session.delete(entity);
	        
			closeSession();
			return true;
		}
		return false;
	}
	
	public void update(Client entity){

		openSession();
		
		 	session.saveOrUpdate(entity);;
        
		closeSession();
	}

	
	public List<Client> view(){
	
		openSession();
		
			String q = "FROM Client";
			org.hibernate.Query query = session.createQuery(q);
			List<Client> list = query.list();
			
			for(int i = 0; i< list.size();i++)
			{
				System.out.println(list.get(i));
			}

		closeSession();
	
		return list;
	}

	public boolean searchByKey(String CNP){
		boolean ok = false;

		openSession();
		
		String q = "FROM Client";
		org.hibernate.Query query = session.createQuery(q);
		List<Client> list = query.list();
		
		
		ok = list.contains(session.get(Client.class, CNP));
		
		closeSession();
		
		return ok;
	}
	
	@SuppressWarnings("unchecked")
	public Client getByKey(String CNP){
		
		Client client2 = null;
		
		openSession();
		Criteria cr = session.createCriteria(Client.class);
		cr.add(Restrictions.eq("CNP", CNP));
		List<Client> list = (List<Client>)cr.list();
		
		if(list.size()!=0){
			client2 = list.get(0);
		}
		else
			client2 = null;
		closeSession();
		
		return client2;
				
	}
	
	
	//******************** Validations *************************
	
	public boolean validCNP(String CNP){
		boolean ok = true;
		
		if(CNP.length() != 13) return false;
		
		int key[] = {2, 7, 9, 1, 4, 6, 3, 5, 8, 2, 7, 9};
		int sum = 0;
		
		for(int i = 0; i< key.length; i++){
			sum+=key[i] * Character.getNumericValue(CNP.charAt(i));
		}
		
		int lastDigit = sum - 11 * ((int) sum/11);
		if(lastDigit == 10)
			lastDigit = 1;
		if(Character.getNumericValue(CNP.charAt(CNP.length()-1)) == lastDigit)
			ok = true;
		else
			ok = false;
		return ok;
	}
	
	public boolean okToAdd(Client client){
		boolean ok = true;
		
		if(searchByKey(client.getCNP()))
			ok = false;
		if(!validCNP(client.getCNP()))
			ok = false;
		
		return ok;
	}
	
	public boolean okToDelete(Client client){
		boolean ok = false;
		
		if(searchByKey(client.getCNP()))
			ok = true;
		
		return ok;
	}
	
}
