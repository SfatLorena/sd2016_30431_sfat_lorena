package data.source.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import business.layer.models.Account;
import business.layer.models.Activity;
import business.layer.models.Administrator;
import business.layer.models.Employee;

public class AdministratorDAO extends SessionImpl{
	
	public void add(Administrator entity){

		if(okToAdd(entity))
		{
			openSession();
			System.out.println("Session Opened for ADD.");
			 	session.save(entity);
			 	System.out.println("Session Closed for ADD.");
			closeSession();
		}
	}
	
	public void delete(Administrator entity){
		if(okToDelete(entity)){
			openSession();
			
			 	session.delete(entity);
	        
			closeSession();
		}
	}
	
	public void update(Administrator entity){

		openSession();
		
		 	session.saveOrUpdate(entity);;
        
		closeSession();
	}
	
	public List<Administrator> view(){
		
		openSession();
		
			@SuppressWarnings("unchecked")
			List<Administrator> list = (List<Administrator>) session.createCriteria(Administrator.class).list();
			
			for(int i = 0; i< list.size();i++)
			{
				System.out.println(list.get(i));
			}

		closeSession();
	
		return list;
	}
	
	public boolean searchByKey(String username){
		boolean ok=true;
		
		openSession();
		
		String q = "FROM Administrator";
		org.hibernate.Query query = session.createQuery(q);
		List<Administrator> list = query.list();
				
		ok = list.contains(session.get(Administrator.class, username));
		
		closeSession();
		
		return ok;
	}
	
	public Administrator getbyKey(String username){
		openSession();
		Administrator admin;
		Criteria cr = session.createCriteria(Administrator.class);
		cr.add(Restrictions.eq("username", username));
		
		if(cr.list().get(0)!= null){
			admin = (Administrator) cr.list().get(0);
		}
		else
			admin = null;
		closeSession();
		return admin;
	}

	// ************** Validations *********************
	
	public boolean okToAdd(Administrator admin){
		boolean ok = true;
		
		if(searchByKey(admin.getUsername()))
			ok = false;
		if(!securePassword(admin.getPassword()))
			ok = false;
	
		return ok;
	}
	
	public boolean okToDelete(Administrator admin){
		boolean ok = false;
		
		if(searchByKey(admin.getUsername()))
			ok = true;
		
		return ok;
	}
	
	public boolean securePassword(String password){
		
		//Requirements: upper & lowercase letters, at least 1 digit,min 7 charasters
		
		boolean upper = false;
		boolean lower = false;
		boolean digit = false;
		int length = 0;
		
		for(char c: password.toCharArray()){
			if(Character.isUpperCase(c))
				upper = true;
			else if(Character.isLowerCase(c))
				lower = true;
			else if(Character.isDigit(c))
				digit = true;
				
			length++;
		}
		
		if(!upper || !lower || !digit ||(length<7))
			return false;
		else
			return true;
	}
}
