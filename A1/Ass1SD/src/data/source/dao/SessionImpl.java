package data.source.dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public abstract class SessionImpl {
	private static SessionFactory factory;
	Transaction tx;
	protected Session session;
	
	private void setFactory(){
		try{
			factory = new Configuration().configure().buildSessionFactory();
		}
		catch(HibernateException exception){
		     System.out.println("Problem creating session factory");
		     exception.printStackTrace();
		}
	}
	
	protected void openSession(){
		setFactory();
		session = factory.openSession();
	    tx = null;
	    tx = session.beginTransaction();
	}
	
	protected void closeSession(){
		tx.commit();
		//if (tx!=null) tx.rollback();
		session.flush();
        session.close(); 
        
	}
}

