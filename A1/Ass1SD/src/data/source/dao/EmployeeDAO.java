package data.source.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import business.layer.models.Account;
import business.layer.models.Activity;
import business.layer.models.Administrator;
import business.layer.models.Employee;

public class EmployeeDAO extends SessionImpl{
	
	public void add(Employee entity){
		if(okToAdd(entity) && securePassword(entity.getPassword())){
			
			openSession();
			System.out.println("Session Opened for ADD.");
			 	session.save(entity);
			 	System.out.println("Session Closed for ADD.");
			closeSession();
		}
	}
	
	public void delete(Employee entity){

		if(okToDelete(entity)){
			openSession();
			
			 	session.delete(entity);
	        
			closeSession();
		}
	}
	
	public void update(Employee entity){

		if(validAmount(entity.getSalary())){
			openSession();
			
			 	session.saveOrUpdate(entity);;
	        
			closeSession();
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Employee> view(){
		
		openSession();
		
			List<Employee> list = (List<Employee>) session.createCriteria(Employee.class).list();
			
			for(int i = 0; i< list.size();i++)
			{
				System.out.println(list.get(i));
			}

		closeSession();
	
		return list;
	}
	
	public boolean searchByKey(String username){
		boolean ok=true;
		
		openSession();
		
		Criteria cr = session.createCriteria(Employee.class);
		cr.add(Restrictions.eq("username", username));
		
		List<Employee> list = cr.list();
		
		
		ok = !list.isEmpty();
		
		closeSession();
		
		return ok;
	}
	
	public Employee getByKey(String username){

		Employee employee;
		
		openSession();
		Criteria cr = session.createCriteria(Employee.class);
		cr.add(Restrictions.eq("username", username));
		
		if(cr.list().get(0)!= null){
			employee = (Employee) cr.list().get(0);
		}
		else
			employee = null;
		closeSession();
		
		return employee;
	}

	//********************* Validations ****************************
	
	public boolean validAmount(double amount){
		if(amount>0)
			return true;
		else
			return false;
	}
	
	public boolean okToAdd(Employee employee){
		boolean ok = true;
		
		if(searchByKey(employee.getUsername()))
			ok = false;
		if(!securePassword(employee.getPassword()))
			ok = false;

		return ok;
	}
	
	public boolean okToDelete(Employee employee){
		boolean ok = false;
		
		if(searchByKey(employee.getUsername()))
			ok = true;
		
		return ok;
	}
	
	public boolean securePassword(String password){
		
		//Requirements: upper & lowercase letters, at least 1 digit,min 7 charasters
		
		boolean upper = false;
		boolean lower = false;
		boolean digit = false;
		int length = 0;
		
		for(char c: password.toCharArray()){
			if(Character.isUpperCase(c))
				upper = true;
			else if(Character.isLowerCase(c))
				lower = true;
			else if(Character.isDigit(c))
				digit = true;
				
			length++;
		}
		
		if(!upper || !lower || !digit ||(length<7))
			return false;
		else
			return true;
	}
}
