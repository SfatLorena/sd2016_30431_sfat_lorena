package business.layer.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "activity")

public class Activity {
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idactivity;
	
	@OneToOne
	@JoinColumn(name = "username")
	private Employee employee;
	
	private String name;

	private String description;

	private Date date;
	//private Date end = new Date();

	//private Date start = new Date();
	
	
	public String getName() {
		return name;
	}
	public Employee getEmployee() {
		return employee;
	}
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getIdactivity() {
		return idactivity;
	}
	
	@Override
	public String toString(){
		return "[Activity]"+getName()+"("+getDate()+": "+getDescription()+").";
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
}
