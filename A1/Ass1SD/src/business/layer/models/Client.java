package business.layer.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "client")
public class Client {
	@Id
	private String CNP;
	private String name;
	private String surname;
	private String address;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getCNP() {
		return CNP;
	}
	public void setCNP(String cNP) {
		CNP = cNP;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	@Override
	public String toString(){
		return "[Client:"+getCNP()+"]"+getName()+" "+getSurname()+"-"+getAddress()+".";
	}
	
	@Override
	public boolean equals(Object o){
		boolean ok = false;
		Client client = (Client) o;
		
		if(this.CNP.equals(client.getCNP())){
			if(this.name.equals(client.getName())){
				if(this.surname.equals(client.getSurname())){
					if(this.address.equals(client.getAddress())){
						ok = true;
					}					
				}
			}
		}
		
		return ok;
	}
}
