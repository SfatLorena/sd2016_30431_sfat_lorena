package business.layer.models;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

@Entity
@Table(name = "account")
public class Account {
	@Id
	private Long idaccount;
	
	public void setIdaccount(Long idaccount) {
		this.idaccount = idaccount;
	}
	@OneToOne
	@JoinColumn(name = "idclient")
	private Client client;
	
	public void setClient(Client client) {
		this.client = client;
	}

	public Account(){}
	
	public Account(Client client, Date d){
		this.client = client;
		creation_date = d;
	}
	
	public Long getIdaccount() {
		return idaccount;
	}
	public Client getClient() {
		return client;
	}
	@Column
	@Type(type="date")
	private Date creation_date;
	private double ammount;
	
	public Date getCreation_date() {
		return creation_date;
	}
	public void setCreation_date() {
		creation_date = new Date();
	}
	public double getAmmount() {
		return ammount;
	}
	public void setAmmount(double ammount) {
		this.ammount = ammount;
	}

	@Override
	public String toString(){
		return "[Account"+getIdaccount()+"]Client ID:"+getClient()+".";
	}
	
}
