package run;

import java.awt.EventQueue;

import mvc.controller.LogInController;
import mvc.model.LogInModel;
import mvc.view.LogInView;

public class Main {

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LogInView frame = new LogInView();
					LogInModel model = new LogInModel();
					
					LogInController controller = new LogInController(model, frame);
					
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

}
