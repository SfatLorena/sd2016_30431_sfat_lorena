package mvc.view;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;

public class LogInView extends JFrame{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textField;
	private JPasswordField passwordField;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private String username;
	private String password;
	private JButton loginButton;


	/**
	 * Create the frame.
	 */
	public LogInView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setTitle("Log In");
		
		textField = new JTextField();
		textField.setBounds(147, 22, 260, 31);
		contentPane.add(textField);
		textField.setColumns(10);
				
		JTextArea txtrUsername = new JTextArea();
		txtrUsername.setText("Username");
		txtrUsername.setBounds(10, 25, 127, 28);
		contentPane.add(txtrUsername);
		
		JTextPane txtpnPassword = new JTextPane();
		txtpnPassword.setText("Password");
		txtpnPassword.setBounds(10, 83, 127, 31);
		contentPane.add(txtpnPassword);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(147, 83, 260, 31);
		contentPane.add(passwordField);
		
		JTextArea txtrLoginAs = new JTextArea();
		txtrLoginAs.setText("Login as:");
		txtrLoginAs.setBounds(10, 167, 127, 22);
		contentPane.add(txtrLoginAs);
		
		loginButton = new JButton("LogIn");
		loginButton.setBounds(166, 220, 121, 31);

		contentPane.add(loginButton);
	}

	
	public String getPasswordField() {
		return String.valueOf(passwordField.getPassword());
	}


	public String getUsername() {
		return textField.getText();
	}

	public void addLoginListener(ActionListener listenForLoginButton){
		loginButton.addActionListener(listenForLoginButton);
	}
}
