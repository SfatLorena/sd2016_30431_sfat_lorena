package mvc.utils;

import java.util.List;
import mvc.model.BookModel;

import java.io.FileWriter;
import java.io.IOException;

public class CSVwriter implements Writer{
	public void writeOutOfStockBooks(List<BookModel> books){
		try
		{
		    FileWriter writer = new FileWriter("OutOfStock.csv");
			 
		    //header
		    writer.append("Title");
		    writer.append(',');
		    writer.append("Author");
		    writer.append(',');
		    writer.append("Genre");
		    writer.append('\n');

		    for(BookModel book: books){
			    writer.append(book.getTitle());
			    writer.append(',');
			    writer.append(book.getAuthor());
			    writer.append(',');
			    writer.append(book.getGenre());
			    writer.append('\n');
		    }
				
		    writer.flush();
		    writer.close();
		}
		catch(IOException e)
		{
		     e.printStackTrace();
		} 
	    
	}
}
