package mvc.utils;

import java.util.List;

import mvc.model.BookModel;

public interface Writer{
	public void writeOutOfStockBooks(List<BookModel> books);
}
