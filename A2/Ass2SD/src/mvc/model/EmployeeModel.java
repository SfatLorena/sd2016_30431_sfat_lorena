package mvc.model;

import java.util.LinkedList;
import java.util.List;

import dom.BookDOM;

public class EmployeeModel {
	private String username;
	private String password;
	private String name;
	private String surname;
	private String address;
	BookDOM bdom;

	public EmployeeModel() {
		bdom = new BookDOM();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean sellBooksByID(int id, int pieces) {
		BookModel bm = bdom.getBook(id + "");
		if (bm != null) {
			bdom.updateBook(id + "", (bm.getQuantity() - pieces) + "", "");
			return true;
		}
		return false;
	}
	
	public boolean sellBooksByTitle(String id, int pieces) {
		BookModel bm = bdom.getBook(id);
		if (bm != null) {
			bdom.updateBook(bm.getID()+"", (bm.getQuantity() - pieces) + "", "");
			return true;
		}
		return false;
	}
	
	public List<BookModel> viewBookbyTitle(String title){
		return bdom.searchForBookByTitle(title);
	}
	
	public List<BookModel> viewBookbyAuthor(String author){
		return bdom.searchForBookByAuthor(author);
	}
	
	public List<BookModel> viewBookbyGenre(String genre){
		return bdom.searchForBookByGenre(genre);
	}
	
	public BookModel getBook(String id){
		return bdom.getBook(id);
	}
	


	@Override
	public String toString() {
		return "[Employee]" + getName() + " " + getSurname();
	}

}
